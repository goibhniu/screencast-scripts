#!/bin/sh

DATE=`date +%Y%m%d`
TIME=`date +%Hh%M`

SCREENCASTDIR="/mnt/bulk/Project/screencasts/recordings"

# Start ffmpeg in the background so that we can monitor jack_capture
# from the terminal
ffmpeg -loglevel panic -an -f x11grab -s 1280x720 -i :1.0 -vpre libvpx-720p -b:v 3900k -y $SCREENCASTDIR/screencast_video_$DATE-$TIME.webm &

# Webcam too
# ffmpeg -loglevel panic -an -f video4linux2 -s 1280x1024 -i /dev/video0 -vpre libvpx-720p -b:v 3900k -y $SCREENCASTDIR/screencast_video_webcam_$DATE-$TIME.webm &

# Alternatively, with vlc
vlc -q v4l2:///dev/video0:width=640:height=480 :sout='#transcode{vcodec=mp1v,vb=1024,fps=30}:duplicate{dst=standard{access=file,mux=ps,dst='$SCREENCASTDIR'/screencast_video_webcam_'$DATE'-'$TIME'.mpg},dst=display{noaudio}}' &

jack_capture -b 24 -mc $SCREENCASTDIR/screencast_audio_$DATE-$TIME.wav 

pkill ffmpeg
pkill vlc
