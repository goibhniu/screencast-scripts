# Read the relevant Environment vars from the display manager
ENV=`systemctl show display-manager.service -p Environment | cut -f2- -d"="`

XBINPATH=`type -P X`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

for VAR in $ENV
do
    export $VAR
done

$XBINPATH -config $DIR/nested.conf -noreset -retro :1 -logfile $DIR/log -modulepath /var/run/current-system/sw/lib/xorg/modules,/var/run/current-system/sw/lib/xorg/modules/drivers -xkbdir /etc/X11/xkb
